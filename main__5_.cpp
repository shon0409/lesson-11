#include "threads.h"

int main()
{
	call_I_Love_Threads();

	//part c
	vector<int> primes1;
	getPrimes(0, 1000, primes1);
	primes1 = callGetPrimes(0, 10000);
	primes1 = callGetPrimes(0, 1000000);
	std::cout << "\n\n";

	//part e
	callWritePrimesMultipleThreads(0, 1000, "C:\\Users\\user\\Documents\\magshimim\\lesson11file.txt", 3);
	callWritePrimesMultipleThreads(0, 10000, "C:\\Users\\user\\Documents\\magshimim\\lesson11file.txt", 3);
	callWritePrimesMultipleThreads(0, 1000000, "C:\\Users\\user\\Documents\\magshimim\\lesson11file.txt", 3);
	
	system("pause");
	return 0;
}
//string derection = "C:\Users\user\Documents\magshimim\lesson11file.txt";