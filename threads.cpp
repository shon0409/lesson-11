#include "threads.h"
#include <thread>
#include <iostream>
#include <time.h>
#include<math.h>
#include <iomanip>

void I_Love_Threads()//The function prints "I Love Threads" and descending line
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()//The function activates the function I_Love_Threads by using thread
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)//The function auxiliary function the function gets and prints the vector values. Each entry in a row
{
	for (unsigned int i = 0; i <= primes.size(); i++)
	{
		std::cout << primes[i] << std::endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)//The function gets a specific range of numbers and returns the primes by a vector type.
{
	std::vector<int>::iterator it;

	it = primes.begin();

	for (int i = begin; i <= end; i++)
	{
		if (isPrime(i) == true)
		{
			it = primes.insert(it, i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)//The function using and returns the vector of the primesThe function calculates the time it took to threads
{
	vector<int> primes;
	clock_t  timeStart;
	clock_t timeEnd;
	double timeNeto;

	timeStart = clock();
	std::thread t2(getPrimes, std::ref(begin), std::ref(end), std::ref(primes));
	t2.join();
	timeEnd = clock();
	timeNeto = ((double)(timeEnd - timeStart)) / (CLOCKS_PER_SEC);//param of time.h

	std::cout << "The time that took to the process is:" << timeNeto << "seconds" << std::endl;

	return primes;
}


void writePrimesToFile(int begin, int end, ofstream& file)//The function gets a specific range of numbers and doing the file and writes the file to the primes in the range
{
	if (file.is_open())
	{
		for (int i = begin; i < end; i++)
		{
			if (isPrime(i))
			{
				file << i << " ";
			}
		}

		file.close();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)//The function Gets a specific range of numbers divides the range into N smaller ranges creates N threads, each thread Run the writePrimesToFile function on its scope. The function calculates the total time
{
	clock_t  timeStart;
	clock_t timeEnd;
	double timeNeto;
	ofstream file(filePath);
	int temp = begin;

	timeStart = clock();
	for (int i = 0; i < N; i++)
	{
		std::thread t3(writePrimesToFile, std::ref(temp), (temp + ((end - begin) / N)), std::ref(file));
		t3.join();
		temp = (temp + ((end - begin)/N)) + temp + 1;
	}
	timeEnd = clock();
	timeNeto = ((double)(timeEnd - timeStart)) / (CLOCKS_PER_SEC);//param of time.h

	std::cout << "The time that took to the process is:" << timeNeto << "seconds" << std::endl;
}

bool isPrime(int num)
{
	for (int i = 2; i < (num / 2); i++)
	{
		if (num%i == 0)
		{
			return false;
		}
	}
	return true;
}