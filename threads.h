#include <string>
#include <fstream>
#include <iostream>
#include<vector>

using namespace std;

/*
The function prints "I Love Threads" and descending line
input: none
output: none
*/
void I_Love_Threads();

/*
The function activates the function I_Love_Threads by using thread
input: none
output: none
*/
void call_I_Love_Threads();

/*
The function auxiliary function the function gets and prints the vector values. Each entry in a row. No extras
input: primes
output: none
*/
void printVector(vector<int> primes);

/*
The function gets a specific range of numbers and returns the primes by a vector type.
input: begin, end and primes
output: none
*/
void getPrimes(int begin, int end, vector<int>& primes);

/*
The function using and returns the vector of the primesThe function calculates the time it took to threads
input: begin and end
output: 
*/
vector<int> callGetPrimes(int begin, int end);

/*
The function gets a specific range of numbers and doing the file and writes the file to the primes in the range
input: begin, end and file
output: none
*/
void writePrimesToFile(int begin, int end, ofstream& file);

/*
The function Gets a specific range of numbers divides the range into N smaller ranges creates N threads, each thread Run the writePrimesToFile function on its scope.
The function calculates the total time
input: begin, end, filePath and N 
output: none
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);

/*
The function cheack if the number is prime.
input: number
output: boolian param
*/
bool isPrime(int num);